import { BrowserRouter as Router } from 'react-router-dom';
import React, { SFC } from 'react';
import './App.css';
import Root from './Root';
import * as log from 'loglevel';

import { AuthContext, AuthService } from './auth';

const authService = new AuthService();

type LogLevels = Record<string, log.LogLevelDesc>;
const logLevels: LogLevels = {
  trace: 'trace',
  debug: 'debug',
  info: 'info',
  warn: 'warn',
  error: 'error',
  silent: 'silent',
};

log.setLevel(logLevels[process.env.REACT_APP_LOG_LEVEL || 'info']);
log.info(`Log level initialized to ${log.getLevel()}`);

const App: SFC<{}> = () => (
  <AuthContext.Provider value={authService}>
    <Router>
      <Root />
    </Router>
  </AuthContext.Provider>
);

export default App;
