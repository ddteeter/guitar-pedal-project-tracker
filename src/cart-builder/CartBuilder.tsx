import React, { SFC } from 'react';
import { Authenticated } from '../auth';
import { RouteComponentProps } from 'react-router-dom';

type Props = RouteComponentProps;

const CartBuilder: SFC<Props> = () => {
  return (
    <Authenticated>
      <div>Cart Builder</div>
    </Authenticated>
  );
};

export default CartBuilder;
