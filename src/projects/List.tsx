import React, { SFC } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import Project from './model/Project';

type Props = {
  projects: Project[];
} & RouteComponentProps;

const List: SFC<Props> = ({ projects, match }) => (
  <ul>
    {projects.map(project => {
      return (
        <li key={project.id}>
          <Link to={`${match.url}/${project.id}`}>{project.name}</Link>
        </li>
      );
    })}
  </ul>
);

export default List;
