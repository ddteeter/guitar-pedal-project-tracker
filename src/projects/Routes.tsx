import React, { SFC } from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import Home from './Home';
import Create from './Create';
import View from './View';
import Project from './model/Project';
import { Authenticated } from '../auth';

type Props = {
  onNewProject(project: Project): void;
  projects: Project[];
} & RouteComponentProps;

const Routes: SFC<Props> = ({ onNewProject, projects, match }) => (
  <Authenticated>
    <Switch>
      <Route
        path={`${match.path}/create`}
        render={props => <Create {...props} onNewProject={onNewProject} />}
      />
      <Route
        path={`${match.path}/:projectId`}
        render={props => (
          <View
            {...props}
            projectId={props.match.params.projectId || 'invalid-path'}
          />
        )}
      />
      <Route
        path={`${match.path}`}
        render={props => <Home {...props} projects={projects} />}
      />
    </Switch>
  </Authenticated>
);

export default Routes;
