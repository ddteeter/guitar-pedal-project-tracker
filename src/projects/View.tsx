import React, { useState, SFC, useEffect, useContext } from 'react';
import Project from './model/Project';
import ProjectsService from './service/ProjectsService';
import { User } from 'firebase';
import { UserContext } from '../auth';

type Props = {
  projectId: string;
};

const projectsService = new ProjectsService();

const View: SFC<Props> = ({ projectId }) => {
  const [loading, setLoading] = useState(true);
  const [project, setProject] = useState<Project | undefined>(undefined);
  const user = useContext(UserContext)!;

  useEffect(
    () => {
      const fetchProject = async () => {
        setLoading(true);
        setProject(await projectsService.getProject(user, projectId));
        setLoading(false);
        // TODO: Error handling
      };

      fetchProject();
    },
    [projectId]
  );

  return <h2>{loading ? 'Loading...' : project!.name}</h2>;
};

export default View;
