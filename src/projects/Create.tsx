import React, { SFC, useState, useRef, useContext } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import UserContext from '../auth/context/UserContext';
import Components from '../components/Components';
import ProjectsService from './service/ProjectsService';
import Project from './model/Project';
import Component from '../components/component/model/Component';
import UnpersistedProject from './model/UnpersistedProject';
import * as log from 'loglevel';

type Props = {
  onNewProject(project: Project): void;
} & RouteComponentProps;

const initialState = {
  name: '',
  isFormValid: false,
};

type State = Readonly<typeof initialState>;

const projectsService = new ProjectsService();

const Create: SFC<Props> = props => {
  const [formState, setFormState] = useState({
    name: '',
    isFormValid: false,
  });

  const user = useContext(UserContext)!;

  const fileBuildDocsInput = useRef<HTMLInputElement>(null);

  const onChange = (
    event: React.FormEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    setFormState({
      [event.currentTarget.name]: event.currentTarget.value,
      isFormValid: event.currentTarget.form
        ? event.currentTarget.form.checkValidity()
        : initialState.isFormValid,
    } as State);
  };

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (
      fileBuildDocsInput !== null &&
      fileBuildDocsInput.current !== null &&
      fileBuildDocsInput.current.files !== null
    ) {
      try {
        let project = await projectsService.createProject(
          // Unsafe use -- we expect to be wrapped in Authenticated component
          user,
          new UnpersistedProject(
            formState.name,
            fileBuildDocsInput.current.files
          ),
          fileBuildDocsInput.current ? fileBuildDocsInput.current.files : null
        );
        props.onNewProject(project);
        props.history.push('/projects');
      } catch (e) {
        log.error('Unable to save proejct', e);
      }
    } else {
      log.error(
        'Unable to save project -- at least one build doc must be configured'
      );
    }
  };

  const onComponentSave = async (component: Component): Promise<Component> => {
    return new Component();
  };

  return (
    <div>
      <h2>Create a New Project</h2>
      <form id="projectForm" onSubmit={onSubmit}>
        <label htmlFor="name">
          Name
          <input
            type="text"
            id="name"
            name="name"
            required={true}
            maxLength={256}
            size={64}
            value={formState.name}
            onChange={onChange}
          />
        </label>
        <label htmlFor="fileBuildDocs">
          Build Documents
          <input
            type="file"
            name="fileBuildDocs"
            id="fileBuildDocs"
            multiple={true}
            ref={fileBuildDocsInput}
            required={true}
            min={1}
          />
        </label>
      </form>

      <Components components={[]} onComponentSave={onComponentSave} />
      <button type="submit" form="projectForm">
        Save
      </button>
    </div>
  );
};

export default Create;
