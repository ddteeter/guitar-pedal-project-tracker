import React, { SFC } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import List from './List';
import Project from './model/Project';

type Props = {
  projects: Project[];
} & RouteComponentProps;

const Home: SFC<Props> = props => (
  <div>
    <Link to={`${props.match.url}/create`}>Create New Project</Link>
    <List {...props} />
  </div>
);

export default Home;
