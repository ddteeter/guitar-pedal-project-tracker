class Project {
  readonly id: string;
  readonly name: string;
  readonly buildDocsUrls: string[];

  constructor(id: string, name: string, buildDocsUrls: string[]) {
    this.id = id;
    this.name = name;
    this.buildDocsUrls = buildDocsUrls;
  }
}

export default Project;
