class UnpersistedProject {
  readonly name: string;
  readonly buildDocs: FileList;

  constructor(name: string, buildDocs: FileList) {
    this.name = name;
    this.buildDocs = buildDocs;
  }
}

export default UnpersistedProject;
