import React, { SFC, useContext } from 'react';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import AuthContext from './context/AuthContext';

type Props = RouteComponentProps;

const Authenticated: SFC<Props> = ({ children, location }) => {
  const authService = useContext(AuthContext)!;
  return (
    <div>
      {authService.auth.currentUser ? (
        children
      ) : (
        <Redirect
          to={{
            pathname: '/sign-in',
            state: { from: location },
          }}
        />
      )}
    </div>
  );
};

export default withRouter(Authenticated);
