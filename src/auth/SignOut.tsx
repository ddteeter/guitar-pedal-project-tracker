import React, { SFC, useContext } from 'react';
import AuthContext from './context/AuthContext';

const SignOut: SFC<{}> = () => {
  const authService = useContext(AuthContext)!;

  if (authService.auth.currentUser) {
    return (
      <button type="button" onClick={() => authService.signOut()}>
        Sign Out
      </button>
    );
  } else {
    return null;
  }
};

export default SignOut;
