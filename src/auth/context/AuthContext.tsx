import React from 'react';
import AuthService from '../service/AuthService';

const AuthContext = React.createContext<AuthService | undefined>(undefined);

export default AuthContext;
