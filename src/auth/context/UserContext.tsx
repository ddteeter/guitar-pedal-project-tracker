import React from 'react';
import { User } from '@firebase/auth-types';

const UserContext = React.createContext<User | undefined>(undefined);

export default UserContext;
