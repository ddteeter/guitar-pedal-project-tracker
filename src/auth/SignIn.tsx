import React, { SFC, useContext } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import firebase from 'firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { AuthService, AuthContext } from '.';
import * as log from 'loglevel';

type Props = RouteComponentProps;

const SignIn: SFC<Props> = ({ history, location }) => {
  const authService = useContext(AuthContext)!;

  return (
    <div>
      <h1>Sign In</h1>
      <StyledFirebaseAuth
        uiConfig={{
          signInFlow: 'popup',
          callbacks: {
            signInSuccessWithAuthResult: () => {
              log.trace(
                `Successfully authenticated user, redirecting with state ${
                  location.state
                }`
              );
              history.push(location.state);
              return false;
            },
          },
          credentialHelper: 'none',
          signInOptions: [
            firebase.auth.EmailAuthProvider.PROVIDER_ID,
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          ],
        }}
        firebaseAuth={authService.auth}
      />
    </div>
  );
};

export default SignIn;
