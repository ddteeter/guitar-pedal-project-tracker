import * as log from 'loglevel';

class ServiceConfig {
  static SERVICE_URL = process.env.REACT_APP_FIREBASE_SERVICE_URL;

  static url(path: string) {
    log.trace(
      `Building URL with path ${path} on SERVICE_URL ${
        ServiceConfig.SERVICE_URL
      }`
    );
    return (
      ServiceConfig.SERVICE_URL + (path.startsWith('/') ? path : '/' + path)
    );
  }
}

export default ServiceConfig;
