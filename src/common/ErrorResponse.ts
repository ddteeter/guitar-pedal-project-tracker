class ErrorResponse extends Error {
  readonly code: string;
  readonly body: string;

  constructor(code: string, body: string) {
    super(body);
    this.code = code;
    this.body = body;
  }
}

export default ErrorResponse;
