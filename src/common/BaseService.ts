import ServiceConfig from '../config/ServiceConfig';
import { User } from 'firebase';
import JsonResponse from './JsonResponse';
import ErrorResponse from './ErrorResponse';
import * as log from 'loglevel';

class BaseService {
  async request<T>(
    user: User,
    path: string,
    verb: string,
    body: string | FormData | undefined,
    contentType: string
  ): Promise<JsonResponse<T>> {
    let userToken = await user.getIdToken();
    var requestConfig: RequestInit = {
      method: verb,
      cache: 'no-cache',
      headers: {
        'Content-Type': contentType,
        Authorization: 'Bearer ' + userToken,
      },
    };

    if (!!body) {
      if (contentType === 'application/json') {
        requestConfig.body = JSON.stringify(body);
      } else {
        requestConfig.body = body;
      }
    }

    var result;
    const url = ServiceConfig.url(path);
    try {
      log.trace(`Making request to ${url} with config`, requestConfig);
      let response = await fetch(url, requestConfig);
      if (response.status >= 400) {
        throw new ErrorResponse('HTTP_STATUS', response.status.toFixed(0));
      } else {
        result = new JsonResponse<T>(await response.text());
      }
    } catch (e) {
      log.error(
        `Unable to fulfill request to ${url} with config`,
        requestConfig
      );
      throw new ErrorResponse('HTTP_REQUEST', e);
    }

    return result;
  }

  async jsonRequest<T>(
    user: User,
    path: string,
    verb: string,
    body: string | any | undefined = undefined
  ) {
    let jsonBody =
      typeof body === 'string'
        ? body
        : body !== 'undefined'
        ? JSON.stringify(body)
        : undefined;
    return await this.request<T>(
      user,
      path,
      verb,
      jsonBody,
      'application/json'
    );
  }

  async multipartRequest<T>(
    user: User,
    path: string,
    jsonBodyKey: string,
    body: any,
    filesKey: string,
    files: FileList | null
  ): Promise<JsonResponse<T>> {
    var multipartBody = new FormData();
    multipartBody.append(
      jsonBodyKey,
      typeof body === 'string' ? body : JSON.stringify(body)
    );
    if (files) {
      for (let file of Array.from(files)) {
        multipartBody.append(filesKey, file, file.name);
      }
    }
    return await this.request(
      user,
      path,
      'POST',
      multipartBody,
      'multipart/form-data'
    );
  }
}

export default BaseService;
