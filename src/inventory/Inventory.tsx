import React, { SFC } from 'react';
import { Authenticated } from '../auth';
import { RouteComponentProps } from 'react-router-dom';

type Props = RouteComponentProps;

const Inventory: SFC<Props> = () => {
  return (
    <Authenticated>
      <div>Inventory</div>
    </Authenticated>
  );
};

export default Inventory;
