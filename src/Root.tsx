import {
  withRouter,
  Route,
  Link,
  RouteComponentProps,
  Switch,
} from 'react-router-dom';
import React, { SFC, useState, useEffect, useContext } from 'react';
import { SignIn, SignOut, UserContext, AuthContext } from './auth';
import CartBuilder from './cart-builder/CartBuilder';
import Inventory from './inventory/Inventory';
import Project from './projects/model/Project';
import ProjectRoutes from './projects/Routes';
import { User } from '@firebase/auth-types';

type Props = RouteComponentProps;

const Root: SFC<Props> = ({ history }) => {
  const [projects, setProjects] = useState<Project[]>([]);
  const [user, setUser] = useState<User | undefined>(undefined);
  const [authInitializing, setAuthInitializing] = useState(true);

  const authService = useContext(AuthContext)!;

  const navigateTo = (path: string) => {
    history.push(path);
  };

  const onNewProject = (project: Project) => {
    setProjects([...projects, project]);
  };

  useEffect(() => {
    const unsubscribe = authService.auth.onAuthStateChanged(user => {
      if (!!user) {
        setUser(user);
        setAuthInitializing(false);
        navigateTo('/home');
      } else {
        setUser(undefined);
        setAuthInitializing(false);
        navigateTo('/sign-in');
      }
    });

    return function authStatusCleanup() {
      unsubscribe();
    };
  }, []);

  return (
    <UserContext.Provider value={user}>
      {authInitializing ? (
        <div>Loading...</div>
      ) : (
        <div className="App">
          {!!user ? (
            <nav>
              <Link to="/projects">Projects</Link>
              <Link to="/inventory">Inventory</Link>
              <Link to="/cart-builder">Cart Builder</Link>
              <SignOut />
            </nav>
          ) : null}
          <Switch>
            <Route
              path="/projects"
              render={props => (
                <ProjectRoutes
                  {...props}
                  projects={projects}
                  onNewProject={onNewProject}
                />
              )}
            />
            <Route path="/inventory" component={Inventory} />
            <Route path="/cart-builder" component={CartBuilder} />
            <Route path="/sign-in" component={SignIn} />
          </Switch>
        </div>
      )}
    </UserContext.Provider>
  );
};

export default withRouter(Root);
