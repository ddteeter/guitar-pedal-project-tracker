import React, { SFC, useState, useContext, useEffect } from 'react';
import InlineComponentForm from './InlineComponentForm';
import Component from './model/Component';
import ComponentMetadataService from './service/ComponentMetadataService';
import * as log from 'loglevel';
import ComponentType from './model/ComponentType';
import UnitType from './model/UnitType';
import { UserContext } from '../../auth';

type Props = {
  onComponentSave(component: Component): Promise<Component>;
};

const componentMetadataService = new ComponentMetadataService();

const InlineComponentEditor: SFC<Props> = ({ onComponentSave }) => {
  const [saving, setSaving] = useState(false);
  const [adding, setAdding] = useState(false);
  const [metadataLoading, setMetadataLoading] = useState(true);
  const [error, setError] = useState<string | undefined>(undefined);
  const [componentTypes, setComponentTypes] = useState<ComponentType[]>([]);
  const [unitTypes, setUnitTypes] = useState<UnitType[]>([]);

  const user = useContext(UserContext)!;

  const addComponent = (event: React.FormEvent<HTMLButtonElement>) => {
    setAdding(true);
  };

  const closeAddComponent = () => {
    setAdding(false);
  };

  const saveComponent = async (component: Component) => {
    setSaving(true);
    await onComponentSave(component);
    closeAddComponent();
    setSaving(false);
  };

  useEffect(() => {
    const loadMetadata = async () => {
      try {
        setComponentTypes(
          await componentMetadataService.getComponentTypes(user)
        );
      } catch (e) {
        log.error('Unable to retrieve component metadata', e);
        setError('Unable to retrieve component metadata');
      }
      setMetadataLoading(false);
    };

    loadMetadata();
  }, []);

  return (
    <div>
      {adding ? (
        <div>
          {metadataLoading ? (
            <span>Loading...</span>
          ) : error ? (
            <span>Unable to load component metadata</span>
          ) : (
            <React.Fragment>
              <InlineComponentForm
                onComponentSave={saveComponent}
                componentTypes={componentTypes}
                unitTypes={unitTypes}
              />
              <button type="button" onClick={closeAddComponent}>
                Cancel
              </button>
            </React.Fragment>
          )}
        </div>
      ) : (
        <button type="button" onClick={addComponent}>
          Add Component
        </button>
      )}
    </div>
  );
};

export default InlineComponentEditor;
