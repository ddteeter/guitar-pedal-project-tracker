import React, { SFC, useState } from 'react';
import ComponentMetadataService from './service/ComponentMetadataService';
import Component from './model/Component';
import UnitType from './model/UnitType';
import ComponentType from './model/ComponentType';

type Props = {
  onComponentSave(component: Component): void;
  componentTypes: ComponentType[];
  unitTypes: UnitType[];
};

const InlineComponentForm: SFC<Props> = ({
  onComponentSave,
  componentTypes,
  unitTypes,
}) => {
  const initialFormState = {
    type: undefined as string | undefined,
    value: undefined as string | undefined,
    unit: undefined as string | undefined,
    quantity: undefined as number | undefined,
    isFormValid: false,
  };
  type InitialFormState = typeof initialFormState;
  const [formState, setFormState] = useState(initialFormState);
  const [selectedComponentType, setSelectedComponentType] = useState<
    string | undefined
  >(undefined);

  const onChange = (
    event: React.FormEvent<{
      name: string;
      value: string;
      form: HTMLFormElement | null;
    }>
  ) => {
    setFormState(({
      [event.currentTarget.name]: event.currentTarget.value,
      isFormValid: event.currentTarget.form
        ? event.currentTarget.form.checkValidity()
        : false,
    } as unknown) as InitialFormState);
  };

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onComponentSave({
      type: formState.type,
      value: formState.value,
      unit: formState.unit,
      quantity: formState.quantity,
    });
  };

  const onComponentTypeChange = async (
    event: React.FormEvent<HTMLSelectElement>
  ) => {
    onChange(event);
  };

  return (
    <form onSubmit={onSubmit}>
      <label htmlFor="type">
        Type
        <select id="type" name="type" onBlur={onComponentTypeChange}>
          {componentTypes.map(componentType => (
            <option value={componentType.id}>
              {componentType.displayValue}
            </option>
          ))}
        </select>
      </label>
      <label htmlFor="value">
        Value
        <input
          id="value"
          type="number"
          name="value"
          size={10}
          maxLength={10}
          onChange={onChange}
        />
      </label>
      <label htmlFor="unit">
        Unit
        <select id="unit" name="unit" onBlur={onChange}>
          {(
            componentTypes.find(
              componentType => componentType.id === selectedComponentType
            ) || { unitOptions: [] as UnitType[] }
          ).unitOptions.map(unit => (
            <option value={unit.id}>{unit.displayValue}</option>
          ))}
        </select>
      </label>
      <label htmlFor="quantity">
        Quantity
        <input
          id="quantity"
          type="number"
          name="quantity"
          size={10}
          maxLength={10}
          onChange={onChange}
        />
      </label>
      <button type="submit">Save</button>
    </form>
  );
};

export default InlineComponentForm;
